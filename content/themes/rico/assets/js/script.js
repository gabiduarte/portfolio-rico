$(document).ready(function() {
	var containerSize = $('.details__content').width(),
  		$otherPosts = $('#loop__posts'),
			otherPostsCol = '';

	$('.details__content p:has(img)').each(function() {
		var imgSize = $(this).find('img').width();

		$(this).addClass('details__image details__image--resize');
		if (imgSize < containerSize && imgSize > 0) {
			$(this).removeClass('details__image--resize');
		}
	});

	//SMOOTH SCROLLING
	$('.details__anchor').click(function() {
		$('body').animate({ scrollTop: $('body').offset().top}, 500);
		return false;
	});

	//OTHER POSTS
  if (containerSize > 850) {
  	otherPostsCol = '4';
  } else if (containerSize < 850 && containerSize > 650) {
  	otherPostsCol = '3';
  } else {
  	otherPostsCol = '2';
  }

	if (location.pathname !== '/' && location.pathname !== '/about/') {
		var postID = $('article').attr('class').split(" ")[0];
		$otherPosts.load('/ #loop__posts a:not(".'+ postID + '"):lt('+ otherPostsCol + ')');
  }

  //HAMBURGER MENU
	$('.navigation__menu, .navigation__offset').on('click', function() {
		$(this).toggleClass('navigation__menu--active');
		$('.navigation__bar').toggleClass('navigation__bar--animate');
		$('.navigation__overlay, .navigation__links, .navigation__social').toggleClass('navigation--active');
		$('.navigation__offset').toggle();
		return false;
	});
});